package com.example.yao.toplist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends Activity implements TextView.OnEditorActionListener{

    private EditText et;
    private ArrayAdapter<String> adapter;
    private Menu menu;
    ArrayList<String> MainList;
    ListView lv;
    static final String LIST_STATE = "ListView";
    static final String LIST = "List";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(LIST_STATE, MainList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item, R.id.item);
        et = new EditText(this);
        lv = (ListView) findViewById(R.id.list);
        if (savedInstanceState != null){
            MainList.addAll(savedInstanceState.getStringArrayList(LIST_STATE));
            adapter.addAll(MainList);
            lv.setAdapter(adapter);
        }
        et.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        et.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        et.setImeOptions(EditorInfo.IME_ACTION_DONE);
        et.setOnEditorActionListener((TextView.OnEditorActionListener) this);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            ListView lv = (ListView) findViewById(R.id.list);
            lv.setAdapter(adapter);
            MainList.add(et.getText().toString());
            adapter.add(MainList.size() + ". " + et.getText().toString());
            LinearLayout ml = (LinearLayout)findViewById(R.id.MainLayout);
            ml.removeView(et);
            menu.findItem(R.id.add).setEnabled(true);
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.toggleSoftInput(0, 0);
            et.setText("");
            handled = true;
        }
        return handled;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    public void addElem(){
        et.requestFocus();
        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        LinearLayout ml = (LinearLayout)findViewById(R.id.MainLayout);
        ml.addView(et, 0);
    }

    private void startChallenge() {
        Intent intent = new Intent(this, Challenge.class);
        intent.putExtra(LIST, MainList);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.add:
               item.setEnabled(false);
               addElem();
            break;
            case R.id.place:
                startChallenge();
            break;
        }
        return super.onOptionsItemSelected(item);
    }

}
